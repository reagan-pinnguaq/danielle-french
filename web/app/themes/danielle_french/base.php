<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <div id="master-grid">
      <?php 
        get_template_part('partials/skip-to');    
        do_action('get_header');
        get_template_part('templates/header');
        ?>

        <main id="content" class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->

      <?php
        do_action('get_footer');
        get_template_part('templates/footer');
        wp_footer();
      ?>
    </div>
  </body>
</html>
