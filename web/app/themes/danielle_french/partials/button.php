<?php if ( $button &&  isset($button['title']) ): ?>
	<a href="<?php echo $button['url']; ?>" class="btn <?=sanitize_title($button['title']);?>" target="<?php echo $button['target']; ?>"><?php echo $button['title']; ?></a>
<?php endif; ?>