<section class="form">
	<form id="contact-form" method="POST" action="">
			<input type="hidden" name="action" value="contact_action_hook"/>
			<input type="text" name="first" placeholder="First Name" required/>
			<input type="text" name="last" placeholder="Last Name" required/>
			<input type="email" name="email" placeholder="Email Address" required/>
			<select required>
					<option disabled selected>Inquiry Type</option>
					<option value="wedding">Wedding</option>
					<option value="corporate">Corporate</option>
					<option value="media">Media</option>
					<option value="general">General</option>
			</select>
			<textarea name="message" placeholder="Message" rows="8" required/></textarea><br>
			<input type="text" name="hear" placeholder="How did you hear about us?"/>
			<p>Would you like to join our mailing list?</p>
			<div class="form-radio"><input type="radio" name="newsletter" value="true" id="yes" checked><label for="yes">Yes</label></div>
			<div class="form-radio"><input type="radio" name="newsletter" value="false" id="no"><label for="no">No</label></div>
			<input type="submit" value="Send"/>
			<input type="hidden" name="action">
			<p id="form-response"></p>
	</form>
</section>