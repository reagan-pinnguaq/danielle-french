<?php require( dirname(__DIR__) . '/lib/config/image_sizes.php');?>

<?php if($image && isset($image['url'])):?>
<?php if($image && isset($image['limit']) && $image['limit']){
	// this is a case to force small sizes.
	$image_sizes_custom = array_slice($image_sizes_custom, 0, 2, true);
}?>
<picture>	
	<?php $i = 0;
	 foreach(array_reverse($image_sizes_custom) as $name => $size):?>
	<?php if(isset($image['sizes'][$name])):?>
		
		<?php if(isset($image['header']) && $i == 3):?>
		<source media="(min-width: 300px)" srcset="<?=$image['sizes']['woocommerce_thumbnail'];?>">
		<?php endif;?>
		<source media="(min-width: <?=$size?>px)"  srcset="<?=$image['sizes'][$name];?>">
		
		
	<?php endif;?>
	<?php $i++; endforeach;?>

	<img src="<?=$image['url'];?>" alt="<?php echo $image['alt'];?>" >
	
</picture> 
<?php endif;?>