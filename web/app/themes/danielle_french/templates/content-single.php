<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header class="entry-meta">
    <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
    <?php include(locate_template('templates/blocks/content_block.php'));?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
