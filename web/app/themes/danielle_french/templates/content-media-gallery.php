<?php $image = get_field( 'gallery_item' );?>
<?php $id= get_the_title();?>
<?php if ( $image ): ?>
	<input type="radio" name="gallery-item" id="<?='item-'. $id?>" value="<?='item-'. $id;?>" <?=($i == 0) ? 'checked' : '';?>>
	<?php include(locate_template('partials/picture.php'));?>
	<label for="<?='item-'. $id;?>" style="background-image: url(<?=$image['sizes']['medium']; ?>")></label>
<?php endif; ?>