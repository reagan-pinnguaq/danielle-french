<?php if ( get_row_layout() == 'image_block' ) : ?>
		<?php if ( have_rows( 'image_collection' ) ) : ?>
			<?php while ( have_rows( 'image_collection' ) ) : the_row(); ?>
				<?php $image = get_sub_field( 'image' ); ?>
				<?php if ( $image ): ?>
					<?php include(locate_template('partials/picture.php'));?>
				<?php endif; ?>
			<?php endwhile;?>
	<?php endif; ?>
<?php endif; ?>