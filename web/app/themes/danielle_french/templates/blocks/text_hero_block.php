<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}?>

<section class="text-hero-block" id="<?=sanitize_title(get_sub_field( 'title' ));?>">
	<h2><?php the_sub_field( 'title' ); ?></h2>
	<p><?php the_sub_field( 'text' ); ?></p>
	<?php $button = get_sub_field( 'button_link' ); 
	include(locate_template('partials/button.php'));?>
</section>