<?php if ( ! defined( 'ABSPATH' ) ) {

    exit; // Exit if accessed directly
}
if ( get_row_layout() == 'text_block' ) : ?>

<section class="text-block">
	<?php the_sub_field( 'text' ); ?>
</section>

<?php endif;?>
