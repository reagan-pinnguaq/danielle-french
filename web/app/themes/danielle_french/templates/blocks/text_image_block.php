<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}?>

<?php 
/** 
 * Text Image Block Template
 */
?>
<section class="text-image-block <?php the_sub_field( 'image_shape' ); ?> <?php the_sub_field( 'image_position' ); ?>" >
<?php $image = get_sub_field( 'image' ); ?>
<?php if ( $image ): ?>
	<div class="image-wrap">
		<img src="<?=$image['sizes']['mobile'];?>" alt="<?php echo $image['alt'];?>" >
	</div>
<?php endif; ?>
	<div class="content-wrap">
		<?php the_sub_field( 'text' ); ?>
		<?php $button = get_sub_field( 'button' ); 
		include(locate_template('partials/button.php'));?>
	</div>
</section>