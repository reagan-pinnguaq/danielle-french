<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}?>

<section class="text-images-block">
	<p class="text-center"><?php the_sub_field( 'text' ); ?></p>
	<?php if ( have_rows( 'collection_images' ) ) : ?>
	<div class="collection-container">
		<?php while ( have_rows( 'collection_images' ) ) : the_row(); ?>
			<?php $image = get_sub_field( 'image' ); ?>
			<?php if ( $image ): ?>
				<?php include(locate_template('partials/picture.php'));?>
			<?php endif; ?>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>
</section>