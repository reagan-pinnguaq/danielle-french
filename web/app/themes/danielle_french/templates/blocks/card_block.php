<?php $layout = get_sub_field( 'layout' ); ?>

<section class="card-block <?=$layout;?> <?=(is_page('gatherings') || is_page('accommodation') && $layout == 'grid') ? 'two': 'three';?> " id="<?=sanitize_title(get_sub_field( 'title' ));?>">
<?php if ( have_rows( 'card_'. $layout ) ) : ?>
	<?php while ( have_rows( 'card_'. $layout ) ) : the_row(); ?>
	<div class="card">
		<?php $image = get_sub_field( 'image' ); ?>
		<?php $button = get_sub_field( 'link' ); ?>
		<?php if($button):?>
			<a href="<?=$button['url'];?>" alt="">
				<?php include(locate_template('partials/picture.php'));?>
			</a>
		<?php endif;?>
		<h3><a href="<?=$button['url'];?>" alt=""><?php the_sub_field( 'title' ); ?></a></h3>
		<p><?php the_sub_field( 'text' ); ?></p>
		<?php if($button && isset($button['title']) && !empty($button['title'])):?>
			<?php include(locate_template('partials/button.php'));?>
		<?php endif;?>
	</div>
	<?php endwhile; ?>
<?php endif; ?>
</section>