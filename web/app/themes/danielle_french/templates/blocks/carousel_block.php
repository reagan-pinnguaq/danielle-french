<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>


<?php if ( have_rows( 'slides') ) : ?>
<section class="carousel-block" id="<?=sanitize_title(get_sub_field( 'title' ));?>">
	<?php while ( have_rows( 'slides' ) ) : the_row(); ?>
		<div class="slide fade">
			<?php $image = get_sub_field( 'image' ); ?>
			<?php if ( $image ) : ?>
				<?php include(locate_template('partials/picture.php'));?>
			<?php endif; ?>
			<h4><?php the_sub_field( 'title' ); ?></h4>
			<p><?php the_sub_field( 'text' ); ?></p>
			<?php $button = get_sub_field( 'button' ); 
			include(locate_template('partials/button.php'));?>
		</div>
	<?php endwhile; ?>
	</section>
<?php endif ?>