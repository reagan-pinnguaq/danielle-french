<section class="banner-block" id="<?=sanitize_title(get_sub_field( 'title' ));?>">
	<?php $image = get_sub_field( 'image' ); ?>
	<?php if ( $image ) : ?>
		<?php include(locate_template('partials/picture.php'));?>
	<?php endif; ?>
	<div class="banner-content">
	<h2><?php the_sub_field( 'title' ); ?></h2>
	<p><?php the_sub_field( 'text' ); ?></p>
	<?php $button = get_sub_field( 'button' ); 
	include(locate_template('partials/button.php')); ?>
	</div>
</section>