<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}?>
<?php 
if(!isset($content_id)){
	$content_id = 'content';
}

if ( have_rows( $content_id ) ): ?>
	<?php while ( have_rows( $content_id ) ) : the_row(); 
		
		switch(get_row_layout()):
			case 'text_hero_block' : include(locate_template('templates/blocks/text_hero_block.php')); break;
			case 'text_image_block' : include(locate_template('templates/blocks/text_image_block.php')); break;
			case 'text_images_block' : include(locate_template('templates/blocks/text_images_block.php')); break;
			case 'text_block' : include(locate_template('templates/blocks/text_block.php')); break;
			case 'banner_block' : include(locate_template('templates/blocks/banner_block.php')); break;
			case 'card_block' : include(locate_template('templates/blocks/card_block.php')); break;
			case 'carousel_block' : include(locate_template('templates/blocks/carousel_block.php')); break;
			case 'text_block' : include(locate_template('templates/blocks/text_block.php')); break;
			case 'image_block' : include(locate_template('templates/blocks/image_block.php')); break;
			default: break;
		endswitch;

 endwhile; 
 ?>
<?php endif; ?>