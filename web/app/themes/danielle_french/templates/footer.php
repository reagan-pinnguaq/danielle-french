<footer>
  <section class="footer-signup">
  <?php $image = get_image_option('custom_logo'); $image['small-only'] = true;?>
  <?php include(locate_template('partials/picture.php'));?>
    <h5>Subscribe</h5>
    <p>Sign up to receive news and updates.</p>
    <?php get_template_part('partials/mail-chimp');?>
  </section>
  <address>
    <p>1020 Gray Road, Pontypool, Ontario L0A1K0</p>
    <p><a href="tel:+17052771649">705-277-1649</a> <span class="elipse"></span> <a href="mailto:connect@southpondfarms.ca">connect@southpondfarms.ca</a></p>
  </address>
  <section class="social">
  <?php wp_nav_menu(['theme_location'=>'social_navigation', 'menu_id'=>'social-icons', 'menu_class'=> 'grid', 'container' => false]);?>
  </section>
</footer>
