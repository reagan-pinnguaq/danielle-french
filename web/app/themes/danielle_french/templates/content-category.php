<?php $cats = get_categories();?>
  <?php if(count($cats) > 1):?>

    <section class="post-filter">
    <?php foreach($cats as $cat):?>
      <?php if($cat->slug !== get_option('default_category')):?>
      <label for="<?='type-'. $cat->slug;?>"><?=$cat->name;?></label>
      <?php endif;?>
    <?php endforeach;?>
    </section>

    <section class="card-block grid three posts-categories">
    <?php $j = 0; foreach($cats as $cat):?>
      <?php if($cat->slug !== get_option('default_category')):?>

        <input class="sr-only" type="radio" name="post-filter" id="<?='type-'. $cat->slug;?>" <?=($j == 0 ) ? 'checked' : '';?>>

        <?php $args =array('category_name' => $cat->name, 'post_type'=> 'post', 'post_status'=> true);
        $the_query = new WP_Query( $args );

        if( $the_query->have_posts() ): $i=0;?>
          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
          <?php endwhile;?>
        <?php endif;?>
        <section class="view-more category-<?=$cat->slug;?>">
        <?php $button = array('title' => 'View More', 'target' => '', 'url' => get_term_link($cat));
        include(locate_template('partials/button.php'));?>
        </section>
        <style>
          #<?='type-'. $cat->slug;?>:checked ~ article.category-<?=$cat->slug;?> {
            display:inherit;
          }
          #<?='type-'. $cat->slug;?>:checked ~ section.view-more.category-<?=$cat->slug;?>{
            display:flex;
          }
        </style>
        <?php wp_reset_postdata(); ?>
      <?php $j++; endif;?>
    <?php endforeach; ?>
    
    </section>

  <?php endif;?>