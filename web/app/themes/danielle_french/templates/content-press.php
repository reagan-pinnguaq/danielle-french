<?php 
$display_count = 6;


$args = array(
	'posts_per_page'	=> $display_count,
	'post_type'		=> 'press',
	'meta_key'		=> 'item_type',
	'meta_value'	=> $type,
);
if($type == 'show'){
	$args['order'] = 'ASC';
}
if(isset($offset)){
	$args['offset'] = $offset * $display_count;
}

// query
$the_query = new WP_Query( $args );
if( $the_query->have_posts() ): $i=0;?>
	<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<?php 
		switch($type){
			case 'show' : include(locate_template('templates/content-media-'.$type . '.php')); break;
			case 'press' : include(locate_template('templates/content-media-'. $type . '.php')); break;
			case 'gallery' : include(locate_template('templates/content-media-'. $type . '.php')); break;
			default: break;
		}?>
	<?php $i++; endwhile; ?>
 <?php endif;
 wp_reset_query();
?>