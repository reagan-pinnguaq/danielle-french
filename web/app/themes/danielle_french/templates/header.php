<?php use Roots\Sage\HeaderContent; ?>
<header class="grid">
<?php if( !is_front_page() || !is_product() || !is_cart() || !is_checkout() || !is_account_page()):?>
  <?php HeaderContent\image(); ?>
<?php elseif(is_front_page()):?>
  <?php HeaderContent\seasonalImage();?>
<?php endif; ?>

  <h1 class="branding-primary">
    <a class="grid-content" href="<?=esc_url(home_url('/')); ?>">
    <span class="sr-only"><?php bloginfo('name'); ?></span>
    <?php $image = get_image_option('custom_logo'); $image['small-only'] = true;?>
    <?php include(locate_template('partials/picture.php'));?>
  </a>
  </h1>
  <!-- mobile icon -->
  <section id="mobile-nav">
		<span></span>
		<span></span>
		<span></span>
  </section>
  
  <!-- the current nav system  -->
  <nav id="nav-primary" aria-role="navigation" class="mobile-collapsed">
    <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', "container"=> false, 'menu_class' => '',]);
      endif;
    ?>
  </nav>

  <?php
  if(!is_front_page() || !is_product() || !is_cart() || !is_checkout() || !is_account_page()):?>
  <div id="header-content">
      <h2><?= HeaderContent\title(); ?></h2>
      <p><?=HeaderContent\tagline();?></p>
      <?php $button = get_field( 'Link' ); ?>
      <?php include(locate_template('partials/button.php'));?>
  </div>    
  <?php endif;?>
</header>