<?php if ( have_rows( 'show_item' ) ) : ?>
	<?php while ( have_rows( 'show_item' ) ) : the_row(); ?>
<li class="card">

		<?php $image = get_sub_field( 'image' ); ?>
		<?php $link = get_sub_field( 'video_link' ); ?>
		
		<a href="<?=$link;?>" target="_blank" alt="">
			<?php include(locate_template('partials/picture.php'));?>
		</a>

		<h3><a href="<?=$link;?>" target="_blank" ><?php the_title(); ?></a></h3>
</li>
<?php endwhile; ?>
<?php endif; ?>
