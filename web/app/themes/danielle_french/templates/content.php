<article <?php post_class(['card', 'post-card']); ?>>
  <a href="<?php the_permalink(); ?>">
    <?php $image = get_field( 'image', get_the_ID() ); ?>
    <?php $image['limit'] = true;?>
    <?php include(locate_template('partials/picture.php'));?>
  </a>
  <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
  <footer>
    <?php include(locate_template('templates/entry-meta.php')); ?>
  </footer>
</article>
