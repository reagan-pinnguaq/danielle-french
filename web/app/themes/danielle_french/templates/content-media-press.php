<?php if ( have_rows( 'press_item' ) ) : ?>
	<?php while ( have_rows( 'press_item' ) ) : the_row(); ?>
<li class="card">

		<?php $image = get_sub_field( 'image' ); ?>
		<?php $link = get_sub_field( 'link' ); ?>
		
		<a href="<?=$link;?>" target="_blank" alt="">
			<?php include(locate_template('partials/picture.php'));?>
			<img src="" alt="">
		</a>

		<h3><a href="<?=$link;?>" target="_blank" ><?php the_title(); ?></a></h3>
</li>
<?php endwhile; ?>
<?php endif; ?>
