<?php 
/**
 * Custom Info Package WooCommerce Email Class
 * @uses WooCommerce
 * @extends \WC_Email
 * @see \WC_Email_Customer_Note (used for inspritation)
 * @author Reagan McFadden <reagan@pinnguaq.com>
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WC_Contact_Email' ) ) :
//Add ckass to the woocommerce   

class WC_Contact_Email extends WC_Email
{  
  public $submit_name = '';  
  public $written_message = '';
  public $return_email = '';
  public $submit_subject = '';

  public function __construct(){
    
    $this->id = 'wc_contact_email';
    $this->order_id = '';
    $this->customer_email = false;
    
    $this->description = __("Contact Emails are sent to admin", 'woocommerce');
    $this->template_html  = 'emails/custom.php';
    $this->template_plain = 'emails/plain/custom.php';
    parent::__construct();
  }
  
  public function get_default_subject(){
    return __( ucwords($this->submit_name) . ' - '. ucwords($this->submit_subject) . ' - Contact Form', 'woocommerce');
  }

  public function get_default_heading(){
    return __( 'At ' . date('jS F Y') . ', ' . $this->submit_name . ' wrote:', 'woocommerce');
  }

  public function trigger($name, $email, $message, $subject) {
    $this->setup_locale();
    $this->submit_subject = $subject;
    $this->submit_name = $name;
    $this->return_email = $email;

    $this->title = __(ucwords($subject) . ' - ' . $this->submit_name, 'woocommerce', 'woocommerce');
    $this->heading = $this->get_default_subject();
    $this->subject = $this->get_default_heading();
    $this->recipient = $email; 
    
    $result = false;
    $this->customer_note = $message;
    if ( !$this->is_enabled() && !$this->get_recipient() ) {
      return;
    }
  
    $result = $this->send(get_bloginfo('admin_email'), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments());
    
    $args = [get_bloginfo('admin_email'), $this->get_subject(),  $this->get_headers(), $this->get_attachments(), $this->get_content()];
    $this->restore_locale();
    if ($result){
      return $result;
    }
    var_dump($args);
  }

  /**
   * Overide Functions
   */
  public function get_headers() {
    $header = "Content-Type: " . $this->get_content_type() . "\r\n";
    $header .= 'Reply-to: ' . ucwords($this->submit_name) . ' <' . $this->return_email . '>' . "\r\n";
    return apply_filters( 'woocommerce_email_headers', $header, $this->id, $this->object );
  }

  public function get_attachments() {
    return;
  }

  public function order_downloads(){
    return; //hack.
  }
  /**
   * Get content html.
   *
   * @access public
   * @return string
   */
  public function get_content_html() {
    return wc_get_template_html( $this->template_html, array(
      'order'         => '',
      'email_heading' => $this->get_heading(),
      'customer_note' => $this->customer_note,
      'sent_to_admin' => false,
      'plain_text'    => false,
      'email'			=> $this,
    ) );
  }

  /**
   * Get content plain.
   *
   * @access public
   * @return string
   */
  public function get_content_plain() {
    return wc_get_template_html( $this->template_plain, array(
      'order'         => '',
      'email_heading' => $this->get_heading(),
      'customer_note' => $this->customer_note,
      'sent_to_admin' => false,
      'plain_text'    => true,
      'email'			=> $this,
      'downloadable'
    ) );
  }

}
endif;
