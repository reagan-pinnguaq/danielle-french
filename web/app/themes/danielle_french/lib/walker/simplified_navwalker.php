<?php


class Simplified_Walker_Nav_Menu extends \Walker_Nav_Menu {
    var $state = false;
    var $storage = array();
    
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $this->state = true;
        $output .= '';
    }
  
    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        
        //Create the 
        $output .= '<ul class="sub-menu">';
        // $output .= var_dump($this->storage);
            foreach($this->storage as $item):
                $output .= $item;
            endforeach;
        $output .= '</ul>';

        //reset the state and clear it.
        $this->state = false;
        $this->storage = array();
    }
  
    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $classes = array();
        if( !empty( $item->classes ) ) {
            $classes = (array) $item->classes;
        }
  
        $active_class = '';
        if( in_array('current-menu-item', $classes) ) {
            $active_class = ' class="active"';
        } else if( in_array('current-menu-parent', $classes) ) {
            $active_class = ' class="active-parent"';
        } else if( in_array('current-menu-ancestor', $classes) ) {
            $active_class = ' class="active-ancestor"';
        }
  
        $url = '';
        if( !empty( $item->url ) ) {
            $url = $item->url;
				}
				
				// pr($item);
  
                // $output .= '<li'. $active_class . '><a href="' . $url . '"><img src="'. get_field('icon', $item) .'" alt="'. $item->title .'"><span class="sr-only">' . $item->title . '</span></a></li>';
                if(isset($item->classes[0])){ $itemClasses = $item->classes[0];} else {$itemClasses = '';}
                $item = '<li class="' . $itemClasses . '" ><a target="'. $item->target .'" ' . $active_class . ' href="' . $url . '" >' . $item->title . ' </a></li>';
                ($this->state) ? $this->storage[] = $item : $output .= $item;
                
    }
  
    public function end_el( &$output, $item, $depth = 0, $args = array() ) {

        $output .= '';
    }
  }