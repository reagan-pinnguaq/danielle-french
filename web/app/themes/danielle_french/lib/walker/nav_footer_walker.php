<?php 
namespace Roots\Sage\Walkers\Nav_Footer_Walker;
class Nav_Footer_Walker extends \Walker_Nav_Menu {
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
			$output .= '<ul class="nav-items">';
	}

	public function end_lvl( &$output, $depth = 0, $args = array() ) {
			$output .= '</ul>';
	}

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
			$classes = array();
			if( !empty( $item->classes ) ) {
					$classes = (array) $item->classes;
			}

			$active_class = '';
			if( in_array('current-menu-item', $classes) ) {
			} else if( in_array('current-menu-parent', $classes) ) {
					$active_class = ' class="active-parent"';
			} else if( in_array('current-menu-ancestor', $classes) ) {
					$active_class = ' class="active-ancestor"';
			}

			$url = '';
			if( !empty( $item->url ) ) {
					$url = $item->url;
			}

			
			if($url == '#'){
				$output .= '<div class="section-wrap">';
				$output .= '<li class="title-link">'. $item->title .'</li>';
			} elseif(get_field('footer_icon', $item)){
				$output .= '<li '. $active_class . '><a href="' . $url . '"><img src="'. get_field('footer_icon', $item) .'" alt='. $item->title .'"></a></li>'; 
			} else {
				$output .= '<li '. $active_class . '><a href="' . $url . '">' . $item->title . '</a></li>';
			}
	}

	public function end_el( &$output, $item, $depth = 0, $args = array() ) {
			$output .= '</li>';
			if($item->url == '#'){
				$output .= '</div>';
			}
	}
}