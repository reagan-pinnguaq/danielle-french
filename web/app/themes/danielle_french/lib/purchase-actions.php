<?php

function mysite_pending($order_id) {
    error_log("$order_id set to PENDING");
}
function mysite_failed($order_id) {
    error_log("$order_id set to FAILED");
}
function mysite_hold($order_id) {
    error_log("$order_id set to ON HOLD");
}
function mysite_processing($order_id) {
    error_log("$order_id set to PROCESSING");
}
function mysite_woocommerce_payment_complete( $order_id ) {
	error_log( "Payment has been received for order $order_id" );
}
function mysite_completed($order_id) {
    error_log("$order_id set to COMPLETED");
}
function mysite_refunded($order_id) {
    error_log("$order_id set to REFUNDED");
}
function mysite_cancelled($order_id) {
    error_log("$order_id set to CANCELLED");
}



add_action( 'woocommerce_order_status_pending', 'mysite_pending', 10, 1);
add_action( 'woocommerce_order_status_failed', 'mysite_failed', 10, 1);
add_action( 'woocommerce_order_status_on-hold', 'mysite_hold', 10, 1);
// Note that it's woocommerce_order_status_on-hold, and NOT on_hold.
add_action( 'woocommerce_order_status_processing', 'mysite_processing', 10, 1);
add_action( 'woocommerce_order_status_completed', 'mysite_completed', 10, 1);
add_action( 'woocommerce_order_status_refunded', 'mysite_refunded', 10, 1);
add_action( 'woocommerce_order_status_cancelled', 'mysite_cancelled', 10, 1);
add_action( 'woocommerce_payment_complete', 'mysite_woocommerce_payment_complete', 10, 1 );