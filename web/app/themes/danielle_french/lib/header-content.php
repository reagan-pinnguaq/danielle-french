<?php

namespace Roots\Sage\HeaderContent;
use \DateTime;
/**
 * Page titles
 * 
 * @return string;
 */
function title() {
  return ucwords(discernTitle());
}

function discernTitle(){
  // var_dump(get_theme_mod('danielle_french_home_title'));
  if(is_front_page()){
    // return get_theme_mod('dainelle_french_home_title');
    return get_field('title');
  } elseif (is_home()) {
    if (get_option('page_for_posts', true)) {
      return get_theme_mod('danielle_french_blog_title');
      
    } else {
      return __('Latest Posts', 'sage');
    }
  } elseif(is_page('media')){
    return get_theme_mod('danielle_french_media_title');
  } elseif(is_woocommerce()){
    if(is_shop()){
      return get_theme_mod('woocommerce_store_title');
    } elseif(is_product_category()) {
      return get_the_archive_title();
    } elseif(is_tax()){
      return   get_the_archive_title();
    } else {
      return get_the_title();//fallback or assumed title 
    }
  } elseif (is_archive()) {
    return get_the_archive_title();
  } elseif (is_search()) {
    return sprintf(__('Search Results for %s', 'sage'), get_search_query());
  } elseif (is_404()) {
    return __('Not Found', 'sage');
  } else {
    return get_the_title();//fallback or assumed title 
  }
}

function tagline(){
  return discernTagline();
}

function discernTagline(){
  if(is_woocommerce()){
    if(is_shop()){
       return get_field('text', get_page_by_path('shop'));
    } elseif(is_category()){
      return term_description();
    } elseif(is_tax()){
      return term_description();
    }
  } else { 
    if(is_category()){
      return term_description();    
    } else {
      return get_field('text');
    }
  }
}

/**
 * Page Header Image generator.
 *
 * @return void
 */
function image(){
  $image = discernImage();
  $image['header'] = true; //allow scaling.
  include(locate_template('partials/picture.php'));
  unset($image['header']); //disable scaling.
}
/**
 * @function discernImage function
 *
 * @return object || null
 */
function discernImage() {
  if (is_woocommerce()) {
    if(is_shop() || is_account_page()){
      return get_field('image', get_page_by_path('shop'));
    } elseif(is_product_category()){
      $term = get_term(get_queried_object_id());
      return get_field('image', 'term_'.$term->term_id);
    } elseif( is_tax())  { 
      $term = get_term(get_queried_object_id());
      return  get_field( sanitize_title($term->name) .'_image', get_page_by_path('shop'));
    } elseif(is_product()){
      return get_field('image', get_page_by_path('shop'));
    } else {
      return get_field('image');
    }
  } elseif(is_front_page() || is_404()) {
    return seasonalImage();
  }else{
    return get_field('image');
  }
}

function get_term_image(){
  $term = get_queried_object();
  if($term){
    return get_field('image', $term);
  }
  return;
}

function seasonalImage(){
  $season = discernSeason();
  return get_field( $season. '_image', get_page_by_path('home'));
}

function discernSeason()
{
  $currentMonth=date("m");
  
  if ($currentMonth>="03" && $currentMonth<="05"){
    $season = "spring";
  } elseif ($currentMonth>="06" && $currentMonth<="08"){
    $season = "summer";
  } elseif ($currentMonth>="09" && $currentMonth<="11"){
    $season = "fall";
  } else{
    $season = "winter";
  }
  return $season;
}
/**
 * Undocumented function
 *
 * @return string
 */
function meta_page_content_description()
{
  return wp_filter_nohtml_kses(get_first_content_from_content_blocks());
}

function get_first_content_from_content_blocks(){
   $content = get_field('content');
   $solved = false;
   $i = 0;
   while(!$solved){
     $text = get_value_in_arr('text', $content[$i]);
     $title = get_value_in_arr('title', $content[$i]);
     if($text && $title){
      $text = $title . ' ' . $text;
       $solved = true;
       break;
     }
     $i++;
   }
  return $text;
}

function get_value_in_arr($key, $arr){
  return (isset($arr[$key])) ? $arr[$key] : false;
}