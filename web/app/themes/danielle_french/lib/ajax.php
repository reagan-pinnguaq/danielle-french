<?php 

/**
 * HANDLER FOR AJAX REQUESTS
 */

 use DrewM\MailChimp\MailChimp as MailChimp;
 $mail_state = true;

// PRESS ITEMS
add_action('wp_ajax_pressPostFetch', 'pressPostFetch');
add_action('wp_ajax_nopriv_pressPostFetch', 'pressPostFetch');

function pressPostFetch(){
	$offset = $_POST['offset'];
	$type = $_POST['item_type'];
	$id = $_POST['id'];
	include(locate_template('templates/content-press.php'));	
	die();
}

//BLOG 
add_action('wp_ajax_blogPostFetch', 'blogPostFetch');
add_action('wp_ajax_nopriv_blogPostFetch', 'blogPostFetch');

function blogPostFetch(){
	$display_count = get_option( 'posts_per_page' );
	$offset = $_POST['offset'];
	$cat = (isset($_POST['category']) && $_POST['category'] !== 'undefined') ? $_POST['category'] : false;

	$args = array(
		'posts_per_page'	=> $display_count,
		'offset' => $offset * $display_count,
		'post_type' => 'post',
	);
	if($cat){
		$args['category_name'] = $cat;
	}

	$the_query = new WP_Query( $args );
	if( $the_query->have_posts() ):
		while( $the_query->have_posts() ) : $the_query->the_post();
		get_template_part('templates/content', $the_query->get_post_format()); 
		endwhile;
	else :
		echo 'false';
	endif;
	die();
}

//CONTACT FORM
add_action('wp_ajax_contactPostAction', 'contactPostAction');
add_action('wp_ajax_nopriv_contactPostAction', 'contactPostAction');

function contactPostAction(){
	$name 			= sanitize_text_field($_POST['fname']) . ' ' . sanitize_text_field($_POST['lname']);

	$email 			= $_POST['email'];
	$subject 		= sanitize_text_field($_POST['subject']);
	$body 			= sanitize_text_field($_POST['body']);
	$feedback 	= sanitize_text_field($_POST['feedback']);
	$subscribe 	= $_POST['subscribe'];
	
	if($subscribe){
		$mail_list = get_default_list_id();
		addUserToList($email, $mail_list);
	}
	$mail = new WC_Emails();
  $wc_email_package =$mail->emails["WC_Contact_Email"];
	$result = $wc_email_package->trigger($name, $email, $body, $subject);
	echo $result;
	die();
}

// MAILING LIST
add_action('wp_ajax_mailingSignUp', 'mailingSignUp');
add_action('wp_ajax_nopriv_mailingSignUp', 'mailingSignUp');

function mailingSignUp(){
	$list = $_POST['list'];
	$email = $_POST['email'];
	switch($list){
		case 'wedding': $mail_list = get_wedding_list_id();  break;
		case 'corporate': $mail_list = get_corporate_list_id(); break;
		default: $mail_list = get_default_list_id(); break;
	}
	$list_state = addUserToList($email, $mail_list);
  $mail = new WC_Emails();
  $wc_email_package =$mail->emails["WC_Package_Email"];
	$result = $wc_email_package->trigger($list, $email);
	echo $result;
	die();
}

//mail helpers
function get_default_list_id(){
	$name = 'Mailing List';
	return get_list_id_by_name($name);
}
function get_wedding_list_id(){
	$name = 'Wedding-'. date('Y');
	return get_list_id_by_name($name);
}
function get_corporate_list_id(){
	$name = 'Corporate-'. date('Y');
	return get_list_id_by_name($name);
}
function get_list_id_by_name($name){
	$MailChimp = new MailChimp(getenv('MC_KEY'));
	$result = $MailChimp->get('lists',['count' => 30] );
	if(!$result){
		write_log($MailChimp->getLastError());
		return;
	}
	foreach($result['lists'] as $list){
		if($list['name'] == $name){
			return $list['id'];
		}
	}
}

function get_users_in_list($list_id){
  $MailChimp = new MailChimp(getenv('MC_KEY'));
  $result = $MailChimp->get('lists/' . $list_id . ' /members');
  return ($MailChimp->success()) ? $result : write_log($MailChimp->getLastError());
}

function addUserToList($email, $list_id){
	
	$MailChimp = new MailChimp(getenv('MC_KEY'));
	$result = $MailChimp->post("lists/$list_id/members", [
				'email_address' => $email,
				'status'        => 'subscribed',
	]);

  return ($MailChimp->success()) ? $result : write_log($MailChimp->getLastError());
}

if ( ! function_exists('write_log')) {
  function write_log ( $log )  {
     if ( is_array( $log ) || is_object( $log ) ) {
        error_log( print_r( $log, true ) );
     } else {
        error_log( $log );
     }
  }
}
?>