<?php

function get_image_option($mod){
  $mod_id = get_theme_mod( $mod );
  $meta = wp_get_attachment_metadata( $mod_id );
  $url = wp_get_attachment_image_src( $mod_id , 'full' );
	$sizes = array();
	// var_dump($meta);
  foreach($meta['sizes'] as $attr => $arr){
	 $sizes[$attr] =  dirname($url[0]) .'/'. $arr['file'];
	// var_dump($arr['file']);
  }
  $image = array
  (
    "url" => $url[0],
    "id"  => $mod_id,
    "sizes" => $sizes,
    "alt" => $meta['image_meta']['title']
  );
  return $image;
}