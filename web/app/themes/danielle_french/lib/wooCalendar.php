<?php
/**
 * A Class to pull avaliable bookings from wooBookings and render them as a calendar.
 * 
 * @version 1.1.5
 * @author Reagan McFadden <reagan@pinnguaq.com>
 * 
 * @uses plugin WooCommerce
 * @see https://en-ca.wordpress.org/plugins/woocommerce/
 * 
 * @uses plugin WooCommerce Bookings
 * @see https://woocommerce.com/products/woocommerce-bookings/
 * @uses class plugins\woocommerce-bookings\includes\data-objects\WC_Product_Booking
 * @uses file plugins\woocommerce-bookings\includes\woo-functions.php
 *
 * @param string $month : Format = date('m-Y'), optional
 * 
 */

	class wooCalendar {
		public $current = '';
		public $today ='';
		public $storage  = array();
		private $products = array();
		private $month_start ='';
		private $month_end ='';
		const RETURN_FORMAT = 'd-m-Y';
		const MONTH_FORMAT = 'm-Y';
		const RESULT_LIMIT = 6;

		public function __construct($month = false){
			$this->assign_current_dates($month);
			$this->get_bookable_products();
		}
		/**
		 * Sets current, today, month_start, month_end
		 *
		 * @param string $month
		 * @since 1.0
		 * @return void
		 */
		private function assign_current_dates($month){
			$this->current = ($month) ? DateTime::createFromFormat(self::MONTH_FORMAT, $month)->format(self::RETURN_FORMAT) : date(self::RETURN_FORMAT,  current_time( 'timestamp' ));
			$this->today = date(self::RETURN_FORMAT,  current_time( 'timestamp' ));
			$this->month_start = $this->month_start($this->current);
			$this->month_end = $this->month_end($this->current);
		}
		/**
		 * Fetches all the bookable products using WC Query. Then creates the proper booking objects
		 * and stores them into $this->products
		 * @since 1.0
		 * @return void
		 */
		private function get_bookable_products(){
			$args = $this->get_product_args_array(false);
			$args['posts_per_page'] = -1;
			$query = new WP_Query($args);
			$products = $query->posts;
			foreach($products as $product){
				$this->products[] = new WC_Product_Booking($product->ID);
			}
		}

		
		/**
		 * Takes various taxonomy refernces and returns a premade args array for WPQuery
		 * 
		 * @param bool|string $type : boolean to decern product_group taxonomy or you can provide the product_group. 
		 * @param string $category : category you wish to filter by
		 * @param bool|string $tag : Tags you wish to filter by
		 * @param boolean $featured : Filters for featured only.
		 * @return array : arugments preformated for a WPQuery()
		 * @since 1.1.1
		 */
		private function get_product_args_array($type = true, $category = false,  $tag = false,  $featured = false){

				$tax_query   = WC()->query->get_tax_query();
		
				if($featured){
						$tax_query[] = array(
								'taxonomy' => 'product_visibility',
								'field'    => 'name',
								'terms'    => 'featured',
								'operator' => 'IN',
						);
				}
				if(is_bool($type)){
					($type) ? $operator ='products' : $operator = 'booking';
				} else {$operator =  $type;}
				$tax_query[] = array(
					'taxonomy' => 'product_type',
					'field'    => 'slug',
					'terms'    => $operator
				);
				if($category){
						$tax_query[] = array(
								'taxonomy' => 'product_cat',
								'field'    => 'slug',
								'terms'    => $category
						);
				}
				if($tag){
						$tax_query[] = array(
								'taxonomy' => 'product_tag',
								'field'    => 'slug',
								'terms'    => $tag
						);
				}
				$query_args = array(
						'post_type'           => 'product',
						'post_status'         => 'publish',
						'ignore_sticky_posts' => 1,
						'posts_per_page'      => self::RESULT_LIMIT,
						'orderby'             => 'date',
						'tax_query'           => $tax_query,
				);
				return $query_args;
		}		

		//Callables
		/**
		 * Stores all the avaliable events for the month into the storage array
		 * 
		 * @since 1.0
		 * @return void
		 */
		public function get_month(){
			$this->current = $this->month_start;
			
			for($this->current; strtotime($this->current) <= strtotime($this->month_end); $this->increment_current()){
				$result = $this->get_products_avaliable_for_date($this->current);
				if($result){
					$this->storage[$this->current] = $result;
				}
			}
			if(!empty($this->storage)){
				return $this->storage;
			}
		}
		/**
		 * Returns all avaliable bookings for said date
		 *
		 * @param string $date
		 * @since 1.0
		 * @version 1.0.5
		 * @return array|false 
		 */
		public function get_products_avaliable_for_date($date){
			$avaliable_products = array();
			
			if(empty($this->products)){
				return false;
			}
			
			foreach($this->products as $product){
				$timestamp = strtotime($date);
				$blocks = $product->get_blocks_in_range( $timestamp, $timestamp );
				$result = wc_bookings_get_time_slots( $product, $blocks, array(), 0, $timestamp, $timestamp );
				
				if($result && !empty($result) && isset($result[$timestamp])){

					// var_dump($result);
					$avaliable_products[] = array('name' => $product->get_title(), 'available' => $result[$timestamp]["available"], 'booked' => $result[$timestamp]["booked"], 'permalink' => get_permalink( $product->get_id() ));
				}
			}
			return (!empty($avaliable_products))? $avaliable_products : false;
		}

		// Helpers
		/**
		 * parses date to malleable DateTime
		 *
		 * @param string $date
		 * @return object DateTime
		 */
		private function parse_to_date_object($date){
			return DateTime::createFromFormat(self::RETURN_FORMAT, $date);
		}
		/**
		 * returns the current date in numeric format.
		 *
		 * @return string
		 */
		private function printable_day(){
			return $this->parse_to_date_object( $this->current)->format('d');
		}
		/**
		 * Increments this->current one day
		 * 
		 * @since 1.0
		 * @param bool $desc : optional //increments downwards when set true
		 * @return void
		 */
		private function increment_current($desc = false){
			$d = $this->parse_to_date_object( $this->current);
			($desc) ? $d->modify('-1 day') : $d->modify('+1 day');
			$this->current = $d->format(self::RETURN_FORMAT);
		}
		/**
		 * Finds the first day of any given month
		 *
		 * @param string $month
		 * @since 1.0
		 * @return string
		 */
		private function month_start($month){
			$d = $this->parse_to_date_object( $month);
			$d->modify('first day of this month');
			return  $d->format(self::RETURN_FORMAT);
			
		}
		/**
		 * Returns the last day of any given month
		 *
		 * @param string $month
		 * @since 1.0
		 * @return void
		 */
		private function month_end($month){
			$d = $this->parse_to_date_object( $month);
			$d->modify('last day of this month');
			return  $d->format(self::RETURN_FORMAT);
		}
		/**
		 * Returns an offset int for building the calendar
		 *
		 * @param string $d :format date('d');
		 * @since 1.0
		 * @return int offset
		*/
		private function discern_calendar_offset($d){
			switch($d){
				case 'Sun' : $offset = 0; break;
				case 'Mon' : $offset = 1; break;
				case 'Tue' : $offset = 2; break;
				case 'Wed' : $offset = 3; break;
				case 'Thu' : $offset = 4; break;
				case 'Fri' : $offset = 5; break;
				default: $offset = 6; break;
			}
			return $offset;
		}

		// Render functions
		/**
		 * Front-End template Function. Renders the front end calendar
		 * @since 1.0
		 * @return void
		 */
		public function create_calendar(){		
			$this->render_before_calendar();
			$this->render_previous_month_days();
			$this->render_current_month_days();
			$this->render_next_month_days();
			$this->render_after_calendar();
		}
	/**
	 * Opens the calendar block and renders the menu + headings.
	 * @since 1.1.0
	 * @return void
	 */
		private function render_before_calendar(){
			$month_name = $this->parse_to_date_object( $this->month_start)->format('F Y');
			$prev_month = $this->parse_to_date_object( $this->month_start)->modify('-1 month')->format(self::MONTH_FORMAT);
			$next_month = $this->parse_to_date_object( $this->month_start)->modify('+1 month')->format(self::MONTH_FORMAT);
			?>
			<section id="calendar">
				<ul class="month">
					<li class="prev"><a href="<?=get_permalink() . '?time='. 	$prev_month;?> ">&#10094;</a></li>
					<li class="current-month"><h2><?=$month_name;?></h2></li>
					<li class="next"><a href="<?=get_permalink() . '?time='. 	$next_month;?>">&#10095;</a></li>
				</ul>
				<ul class="calendar-grid weekdays calendar-header-list">
					<li>Sun<span class="mobile-collapsed">day</span></li>
					<li>Mon<span class="mobile-collapsed">day</span></li>
					<li>Tue<span class="mobile-collapsed">sday</span></li>
					<li>Wed<span class="mobile-collapsed">nesday</span></li>
					<li>Thu<span class="mobile-collapsed">rsday</span></li>
					<li>Fri<span class="mobile-collapsed">day</span></li>
					<li>Sat<span class="mobile-collapsed">urday</span></li>
				</ul>
				<ul class="calendar-grid days calendar-content">
		<?php }
		/**
		 * Closes Calendar block up.
		 * @since 1.1.0
		 * @return void
		 */
		private function render_after_calendar(){?>
					</ul>
			</section>
		<?php }
		/**
		 * Renders days before the current month to fill the calendar week.
		 *
		 * @return void
		 */
		private function render_previous_month_days(){
			$start_day = $this->parse_to_date_object( $this->month_start)->format('D');
			$start_offset = $this->discern_calendar_offset($start_day);
			$past = array();

			for($this->current = $this->month_start; $start_offset > 0; $start_offset--){
					$this->increment_current(true);
					$day = $this->printable_day();
					$past[] = $day; 
				}
				foreach(array_reverse($past) as $day):?>
				<li <?=($this->current == $this->today) ? 'class="active not-current-month"' : 'class="not-current-month"';?>>
						<h4><?=$day;?></h4>
				</li>
				<?php endforeach;
		}
		/**
		 * Renders this months days + events happening in them.
		 * @since 1.1.0
		 * @return void
		 */
		private function render_current_month_days(){
			$month = $this->get_month();
			
			for($this->current = $this->month_start; strtotime($this->current) <= strtotime($this->month_end); $this->increment_current()):
			$day = $this->printable_day();
			$past = (strtotime($this->current) < strtotime($this->today)) ? 'past': false;?>
			<li class="<?=($this->current == $this->today) ? 'active' : '';?> <?=($past) ? $past : '';?>" >
				<h4><?=$day;?></h4>
				<?php if(isset($month[$this->current])):?>
				<ul class="date-items collapsed">
				<?php $count = 0; foreach($month[$this->current] as $event):?>
					<li<?php if($event['booked'] > 0 && $event["available"] <= 0):?> class="sold-out" <?php endif;?>><a href="<?=$event['permalink'];?>"><?=$event['name'];?></a></li>
				<?php $count++;  endforeach;?>
				</ul>
				<p><a><?=$count;?> Events</a></p>
				<p class="go-back collapsed"><a>View full Calendar</a></p>
				<?php endif?>
			</li>
			<?php endfor;

		}
		/**
		 * Renders end days to fill the calendar week.
		 * @since 1.1.0
		 * @return void
		 */
		private function render_next_month_days(){
			$end_day = $this->parse_to_date_object( $this->month_end)->format('D');
			$end_offset = 6 - $this->discern_calendar_offset($end_day);
			
			for($this->current = $this->month_end; $end_offset > 0; $end_offset--):
				$this->increment_current();
				$day = $this->printable_day();?>
				<li <?=($this->current == $this->today) ? 'class="active not-current-month"' : 'class="not-current-month"';?>>
						<h4><?=$day;?></h4>
				</li>
			<?php	endfor;
		}
}?>