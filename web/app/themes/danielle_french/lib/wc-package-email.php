<?php 
/**
 * Custom Info Package WooCommerce Email Class
 * @uses WooCommerce
 * @extends \WC_Email
 * @see \WC_Email_Customer_Note (used for inspritation)
 * @author Reagan McFadden <reagan@pinnguaq.com>
 * @version 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

  if ( ! class_exists( 'WC_Package_Emai' ) ) :
    //Add ckass to the woocommerce   

    class WC_Package_Email extends WC_Email
      {  
        var $package = '';  
                
        public function __construct($package = false, $email = false){
          
          
          $defaults = array(
            'order_id'      => '',
            'customer_note' => '',
          );

          $this->id = 'wc_package_email';
          $this->order_id = '';
          $this->customer_email = false;
          
          $this->description = __("Information packages are sent when a user request them.", 'woocommerce');
          $this->template_html  = 'emails/custom.php';
          $this->template_plain = 'emails/plain/custom.php';
          parent::__construct();
        }
        
        public function get_default_subject(){
          return __('An information package was requested', 'woocommerce');
        }

        public function get_default_heading(){
          return __('Your ' . $this->get_blogname(). ' ' . ucwords($this->package) .' Information Package', 'woocommerce');
        }

        public function trigger($package, $email) {
          $this->setup_locale();

          $this->package = $package;
          $this->title = __(ucwords($this->package) . ' Information Package', 'woocommerce', 'woocommerce');
          $this->heading = $this->get_default_subject();
          $this->subject =  $this->get_default_heading();
          $this->recipient = $email; 
          
          $result = false;
          $this->customer_note = $this->get_package_message();
          $this->customer_note  .=  ". \r\n"  . $this->link_attachments();
          // echo 'customer note';
          // var_dump( $this->customer_note);
          if ( !$this->is_enabled() && !$this->get_recipient() ) {
            return;
          }
          
          $result = $this->send($this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments());
          $args = [$this->get_recipient(), $this->get_subject(), $this->get_attachments(),$this->get_attachments(), $this->get_content()];
          $this->restore_locale();
          if ($result){
            return $result;
          }
          var_dump($args);
        }
        
        public function get_package_message(){
          return get_field('email_message', get_page_by_title(strtolower($this->package)));
        }

    /**
     * Overide Function
     *
     * @return void
     */
      public function get_attachments() {
        return add_filter( 'woocommerce_email_attachments', $this->package_woocommerce_attachments(), 10, 3 );
      }

      public function order_downloads(){
        return; //hack.
      }
      /**
       * Undocumented function
       *
       * @return array [collection of pdf links]
       */
      public function package_woocommerce_attachments(){
        return $this->get_acf_attachments();
      }
      /**
       * gets the wedding attachments from the pages attachment repeater
       *
       * @return array
       */
      private function get_acf_attachments(){
        $attachments = array();
        $attach_field = get_field('attachments', get_page_by_path(strtolower($this->package)));
        
        foreach($attach_field as $file):
        
          $attachments[] = $file['attachment'];

        endforeach;
        // echo 'get_acf_attachments';
        // var_dump($attachments);
        return $attachments;
      }     

      private function link_attachments(){
        $str = '';
        $attach = $this->get_acf_attachments();

        foreach($attach as $att){
          $str .= $att . ' ';
        }

        return $str;
      }

      /**
       * Get content html.
       *
       * @access public
       * @return string
       */
      public function get_content_html() {
        return wc_get_template_html( $this->template_html, array(
          'order'         => '',
          'email_heading' => $this->get_heading(),
          'customer_note' => $this->customer_note,
          'sent_to_admin' => false,
          'plain_text'    => false,
          'email'			=> $this,
        ) );
      }

      /**
       * Get content plain.
       *
       * @access public
       * @return string
       */
      public function get_content_plain() {
        return wc_get_template_html( $this->template_plain, array(
          'order'         => '',
          'email_heading' => $this->get_heading(),
          'customer_note' => $this->customer_note,
          'sent_to_admin' => false,
          'plain_text'    => true,
          'email'			=> $this,
          'downloadable'
        ) );
      }

    }
    endif;
