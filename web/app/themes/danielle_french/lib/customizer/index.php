<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;
use \DateTime;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');


function theme_customizer_extend_register($wp_customize){

  //Page titles for existing sections
  $wp_customize->add_setting( 'danielle_french_home_title', array(
    'default'    => 'Welcome to our home',
    'capability' => 'edit_theme_options',
  ));
  
  $wp_customize->add_control('danielle_french_home_title', array(
    
    'section' => 'static_front_page',
    'label' => __('Home Page Title'),
    'description' => __( 'The title that will appear on the home page.' ),
    'input_attrs' => array('placeholder'=> __('Home'), ),
  ));

  $wp_customize->add_setting( 'danielle_french_blog_title', array(
    'default'    => 'Our Blog',
    'capability' => 'edit_theme_options',
  ));
  
  $wp_customize->add_control('danielle_french_blog_title', array(
    
    'section' => 'static_front_page',
    'label' => __('Blog Page Title'),
    'description' => __( 'The title that will appear on the blog page.' ),
    'input_attrs' => array('placeholder'=> __('Store'), ),
  ));

  $wp_customize->add_setting( 'woocommerce_store_title', array(
    'default'    => 'store',
    'capability' => 'edit_theme_options',
  ));
  
  $wp_customize->add_control('woocommerce_store_title', array(
    
    'section' => 'woocommerce_product_catalog',
    'label' => __('Store Title'),
    'description' => __( 'The title that will appear on the store page.' ),
    'input_attrs' => array('placeholder'=> __('Store'), ),
    'prority' => 0,
  ));


$wp_customize->add_section( 'media_page_settings' , array(
  'title'      => __( 'Media Page Settings', 'sage' ),
  'priority'   => 120,
) );

$wp_customize->add_setting( 'danielle_french_media_title', array(
  'default'    => 'Our Show',
  'capability' => 'edit_theme_options',
));

$wp_customize->add_control('danielle_french_media_title', array(
  'section' => 'media_page_settings',
  'label' => __('Media Page Title'),
  'description' => __( 'The title that will appear on the Media page.' ),
  'input_attrs' => array('placeholder'=> __('Media'), ),
));

}

add_action( 'customize_register' ,  __NAMESPACE__ . '\\theme_customizer_extend_register' );
