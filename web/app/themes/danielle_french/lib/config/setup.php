<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  //* Add support for custom logo
  add_theme_support( 'custom-logo', array(
    'header-selector' => '.site-title a',
    'header-text'     => true,
    'flex-height'     => true,
    'flex-width'     => true,
  ) );


  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation'  => __('Primary Navigation', 'sage'),
    'social_navigation'   => __('Social Navigation', 'sage'),
    'social_navigation'   => __('Social Navigation', 'sage'),
    'gallery_navigation'  => __('Gallery Navigation', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  require_once('image_sizes.php');


  /**
   *  Image sizer.
   *  Assumes all images over 700px are banner images.
   *  Banner images a cropped.
   */
  foreach($image_sizes_custom as $size => $width){
    $crop = array('center','center');
     // set 450 max height. Crop from there.
    if($width < 700){
      $height = $width;
      $crop = false;
    } else {
      $height = 450;
    }
    //   $height = 9999;
    //   $crop = false;


      add_image_size( $size, $width, $height, $crop); 
  }

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote','video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
// function widgets_init() {
//   // register_sidebar([
//   //   'name'          => __('Primary', __NAMESPACE__ . '\\sage'),
//   //   'id'            => 'sidebar-primary',
//   //   'before_widget' => '<section class="widget %1$s %2$s">',
//   //   'after_widget'  => '</section>',
//   //   'before_title'  => '<h3>',
//   //   'after_title'   => '</h3>'
//   // ]);

//   // register_sidebar([
//   //   'name'          => __('Footer', __NAMESPACE__ . '\\sage'),
//   //   'id'            => 'sidebar-footer',
//   //   'before_widget' => '<section class="widget %1$s %2$s">',
//   //   'after_widget'  => '</section>',
//   //   'before_title'  => '<h3>',
//   //   'after_title'   => '</h3>'
//   // ]);
// }
// add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page()
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  //slick
  wp_enqueue_script('slick/js', Assets\asset_path('scripts/slick.js'), ['jquery'], null, true);
  wp_enqueue_style('slick/css', Assets\asset_path('styles/slick.css'), false, null); //slick

  //main
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);
  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);

  //ajax
  wp_enqueue_script('sage/ajax', Assets\asset_path('scripts/ajax.js'), ['jquery'], null, true);
  wp_localize_script( 'sage/ajax', 'enqueData', array(
      'ajax_url' => admin_url( 'admin-ajax.php' )
  ));
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


function remove_dashboard_meta() {
  remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}
add_action( 'admin_init', __NAMESPACE__ . '\\remove_dashboard_meta' );

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', __NAMESPACE__ . '\\df_disable_comments_post_types_support');
// Close comments on the front-end
function df_disable_comments_status() {
	return false;
}
add_filter('comments_open', __NAMESPACE__ . '\\df_disable_comments_status', 20, 2);
add_filter('pings_open', __NAMESPACE__ . '\\df_disable_comments_status', 20, 2);
// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', __NAMESPACE__ . '\\df_disable_comments_hide_existing_comments', 10, 2);
// Remove comments page in menu
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', __NAMESPACE__ . '\\df_disable_comments_admin_menu');
// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', __NAMESPACE__ . '\\df_disable_comments_admin_menu_redirect');
// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', __NAMESPACE__ . '\\dashboard', __NAMESPACE__ . '\\normal');
}
add_action('admin_init', __NAMESPACE__ . '\\df_disable_comments_dashboard');
// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', __NAMESPACE__ . '\\wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', __NAMESPACE__ . '\\df_disable_comments_admin_bar');


add_filter( 'get_the_archive_title', function ($title) {

  if ( is_category() || is_tax() ) {
          $title = single_cat_title( '', false );
      } elseif ( is_tag() ) {
          $title = single_tag_title( '', false );
      }
  return $title;
});


function pinnguaq_get_picture_srcs( $image) {
  $mappings = \App\get_image_sizes();
  $arr = array();
  foreach ( $mappings as $type => $size  ) {
      $image_src = wp_get_attachment_image_src( $image, $type );
      $arr[] = '<source srcset="'. $image_src[0] . '" media="(min-width: '. $size['width'] .'px)">';
  }
  return implode( array_reverse ( $arr ) );
}

/**
* Overide function for responseive image inset using media tool
*/
// function responsive_insert_image($html, $id, $caption, $title, $align, $url) {
//   return  '<picture class="align-'. $align . '">
//   <!--[if IE 9]><video style="display: none;"><![endif]-->'
//   . pinnguaq_get_picture_srcs($id) .
//   '<!--[if IE 9]></video><![endif]-->
//   <img src="' . wp_get_attachment_image_src($id, 'full')[0] . '" alt="' . $id .'">
// </picture>';
// }
// add_filter('image_send_to_editor', __NAMESPACE__.'\\responsive_insert_image', 10, 9);