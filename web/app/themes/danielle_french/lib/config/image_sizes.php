<?php 

$image_sizes_custom = array(
    "mobile"         => '300',
    "tablet"         => '750',
    "small-desktop"  => '1100',
    "medium-desktop" => '1500'
  );