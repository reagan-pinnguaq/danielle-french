<?php 

/**
 * Dump file for hard coding features into acf.
 * These are all things you do not want the user to be able to change.
 */


 //POST IMAGE
if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5a8dca16483eb',
		'title' => 'Post - Image',
		'fields' => array(
			array(
				'key' => 'field_5a8dca242697f',
				'label' => 'Image',
				'name' => 'image',
				'type' => 'image',
				'instructions' => 'This is the image that will be used on the home page and in the banner for the home page.',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'medium',
				'library' => 'all',
				'min_width' => 700,
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'side',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	endif;