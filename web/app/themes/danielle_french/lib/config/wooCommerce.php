<?php 

/** CONFIG */

remove_action( 'woocommerce_before_main_content',  'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content',  'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content',  'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content',  'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  echo '<section id="store-content">';
}

function my_theme_wrapper_end() {
  echo '</section>';
}
//Setup 
add_filter('woocommerce_show_page_title',  '__return_false');
remove_action('woocommerce_single_product_summary',  'woocommerce_template_single_title', 5);
//ADD SUPPORT
add_action( 'after_setup_theme',  'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'regen_interactivity' );

function regen_interactivity() {
    // add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

/** Setup product groups.
 * We cannot use product_types namespace because woocommerce uses this
 * for varations, bookings, resources, etc.
 * W
 */
function cptui_register_my_taxes_product_group() {

	/**
	 * Taxonomy: Product Groups.
	 */

	$labels = array(
		"name" => __( "Product Groups", "" ),
		"singular_name" => __( "Product Group", "" ),
	);

	$args = array(
		"label" => __( "Product Groups", "" ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Product Groups",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'product_group', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => true,
	);
	register_taxonomy( "product_group", array( "product" ), $args );
}

add_action( 'init', 'cptui_register_my_taxes_product_group' );

/**
 * Disable product groups from being editable.
 * 
 *
 */
// add_action( 'pre_insert_term', function ( $term, $taxonomy )
// {
//     return ( 'product_group' === $taxonomy )
//         ? new WP_Error( 'term_addition_blocked', __( 'You cannot add terms to this taxonomy' ) )
//         : $term;
// }, 0, 2 );

// remove raiting from woocommerce
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

/**
 * Customize the description. Remove the tabs functionality
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_change_breadcrumb_home_text' );
function jk_change_breadcrumb_home_text( $defaults ) {
    $defaults['home'] = false;
    return $defaults;
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
function woocommerce_template_product_description() {
    wc_get_template( 'single-product/tabs/description.php' );
  }
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );



/**
 * Remove short description.
 *
 * @return void
 */
function remove_short_description() {
 
    remove_meta_box( 'postexcerpt', 'product', 'normal');
     
    }
add_action('add_meta_boxes', 'remove_short_description', 999);




/*************
 * END CONFIG
 ************/

/* Custom email class and action hooks */
add_filter( 'woocommerce_email_classes', 'bootstrap_package_woocommerce_email' );

function bootstrap_package_woocommerce_email( $email_classes ) {
  $dir = get_template_directory();
  include_once ($dir . '/lib/wc-package-email.php');
  include_once ($dir . '/lib/wc-contact-email.php');
  $email_classes['WC_Package_Email'] = new WC_Package_Email();
  $email_classes['WC_Contact_Email'] = new WC_Contact_Email();
  return $email_classes;
}

/**
 * API ACTIONS Hooks
 */

// function transaction_pending($order_id) {
    
// }
// function transaction_failed($order_id) {
    
// }
// function transaction_hold($order_id) {
    
// }
// function transaction_processing($order_id) {
        
// }
// function transaction_woocommerce_payment_complete( $order_id ) {
	
// }
// function transaction_completed($order_id) {
    
// }
// function transaction_refunded($order_id) {
    
// }
// function transaction_cancelled($order_id) {
    
// }

// add_action( 'woocommerce_order_status_pending', 'transaction_pending', 10, 1);
// add_action( 'woocommerce_order_status_failed', 'transaction_failed', 10, 1);
// add_action( 'woocommerce_order_status_on-hold', 'transaction_hold', 10, 1);
// add_action( 'woocommerce_order_status_processing', 'transaction_processing', 10, 1);
// add_action( 'woocommerce_order_status_completed', 'transaction_completed', 10, 1);
// add_action( 'woocommerce_order_status_refunded', 'transaction_refunded', 10, 1);
// add_action( 'woocommerce_order_status_cancelled', 'transaction_cancelled', 10, 1);
// add_action( 'woocommerce_payment_complete', 'transaction_woocommerce_payment_complete', 10, 1 );


/**************************
 *  BEGIN QUERY BUILDERS
 *************************/

 /**
 * @name get_featured_product_args()
 * @description: Takes a get_terms() result and returns 6 featured products
 * @param object $term = result from get_terms()
 * @return array  = a set of arguments for WPQuery();
 */
function get_featured_product_args($term){

    $atts =  array(
        'per_page' => '6',
        'columns'  => '4',
        'orderby'  => 'date',
        'order'    => 'desc',
        'category' => '',  // Slugs
        'operator' => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
    );


    $meta_query  = WC()->query->get_meta_query();
    $tax_query   = WC()->query->get_tax_query();
    $tax_query[] = array(
        'taxonomy' => 'product_visibility',
        'field'    => 'name',
        'terms'    => 'featured',
        'operator' => 'IN',
    );
    $tax_query[] = array(
        'taxonomy' => 'product_group',
        'field'    => 'id',
        'terms'     => $term->term_id
    );

    $query_args = array(
        'post_type'           => 'product',
        'post_status'         => 'publish',
        'ignore_sticky_posts' => 1,
        'posts_per_page'      => $atts['per_page'],
        'orderby'             => $atts['orderby'],
        'order'               => $atts['order'],
        'meta_query'          => $meta_query,
        'tax_query'           => $tax_query,
    );

    return $query_args;
}
/**
 * 
 * get_product_args_array
 * @description: takes various taxonomy refernces and returns a premade args array for WPQuery
 * @param bool $type : boolean to decern product_group. True returns products, false returns events/gatherings
 * @param string $category : category the product belongs to
 * @param string $tag : releavant tag to filter
 * @param boolean $featured : returns featured items.
 * @return array : arugments preformated for a WPQuery()
 * 
 */
function get_product_args_array($type = true, $category = false,  $tag = false,  $featured = false){

    $atts =  array(
        'per_page' => '6',
        'columns'  => '4',
        'orderby'  => 'date',
        'order'    => 'desc',
        'category' => '',
        'operator' => 'IN', // Possible values are 'IN', 'NOT IN', 'AND'.
    );    
    
    $meta_query  = WC()->query->get_meta_query();
    $tax_query   = WC()->query->get_tax_query();

    if($featured){
        $tax_query[] = array(
            'taxonomy' => 'product_visibility',
            'field'    => 'name',
            'terms'    => 'featured',
            'operator' => 'IN',
        );
    }
    ($type) ? $operator ='products' : $operator = 'booking';
    $tax_query[] = array(
        'taxonomy' => 'product_group',
        'field'    => 'slug',
        'terms'    => $operator
    );
    if($category){
        $tax_query[] = array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => $category
        );
    }

    if($tag){
        $tax_query[] = array(
            'taxonomy' => 'product_tag',
            'field'    => 'slug',
            'terms'    => $tag
        );
    }
    
    $query_args = array(
        'post_type'           => 'product',
        'post_status'         => 'publish',
        'ignore_sticky_posts' => 1,
        'posts_per_page'      => $atts['per_page'],
        'orderby'             => $atts['orderby'],
        'order'               => $atts['order'],
        'meta_query'          => $meta_query,
        'tax_query'           => $tax_query,
    );

    return $query_args;
    
    }

