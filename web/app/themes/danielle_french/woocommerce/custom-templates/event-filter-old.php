<?php 
/**
 * Events filter template. 
 * creates a set of inputs that can used to 
 * filter products that are also generated based on cateory type
 */
 global $post;
 $seasons = array('winter', 'spring', 'summer', 'fall'); //assumed default seasons
 ?>
<section class="filter-products" id="#filter">
	<div class="filter-wrapper">
		<label for="featured" class="btn btn-featured">Featured</label><?php foreach($seasons as $season):?><label class="btn btn-featured" for="<?=$season;?>"><?=ucwords($season);?></label><?php endforeach;?>
	</div>
	<?php	// do featured
	$args = get_product_args_array(false, sanitize_title($post->post_title), false, true);
	$products = new WP_Query($args);
	?>
	<!-- inputs -->
	<input type="radio" name="season" id="featured" value="featured" checked><?php foreach($seasons as $season):?><input type="radio" id="<?=$season;?>" name="season" value="<?=$season;?>"><?php endforeach;?>
	<?php
	if ( $products->have_posts() ) : ?>
		<ul class="product-collection card featured">
			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
			<?php wc_get_template_part( 'content', 'product' ); ?>
			<?php endwhile;?> 
		</ul>
	<?php endif; wp_reset_postdata(); ?>
	
	<?php // do seasonal
	foreach($seasons as $season):
	$args = get_product_args_array(false, strtolower($post->post_name), $season, false);
	$products = new WP_Query($args);
	
	if ( $products->have_posts() ) : ?>
		<ul class="product-collection card <?=$season;?>">
			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
			<?php wc_get_template_part( 'content', 'product' ); ?>
			<?php endwhile;?> 
		</ul>
		<?php endif; wp_reset_postdata(); ?>
	<?php endforeach;?>

	<a class="mobile-only btn" href="#filter">back to filters</a>
</section>