<?php 
if(class_exists('wooCalendar')){
	$month = false;
	if(isset($_GET['time'])){
		$month = DateTime::createFromFormat('m-Y', $_GET['time'])->format('m-Y');
	}
	// echo $month;
	$cal = new wooCalendar($month);
	$cal->create_calendar();
}
?>