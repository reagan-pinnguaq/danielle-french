<?php $shop = new WP_Query( array('page_id'=> get_page_by_title('shop')->ID ));

if ($shop->have_posts() ): ?>
  <?php while ($shop->have_posts() ): $shop->the_post(); ?>
    <?php $content_id = 'above_fold_content';?>
		<?php include(locate_template('templates/blocks/content_block.php')); ?>
  <?php endwhile; ?>
  <?php unset( $content_id);?>
<?php endif; ?>
<?php $terms = get_terms(array('taxonomy' => 'product_group', 'parent' => 0)); ?>

<section class="category-menu card-block">
	<?php include(locate_template('woocommerce/custom-templates/product-group-display.php')); ?>	
</section>

<?php 
if ($shop->have_posts() ): ?>
  <?php while ($shop->have_posts() ): $shop->the_post(); ?>
		<?php include(locate_template('templates/blocks/content_block.php')); ?>
  <?php endwhile; ?>
<?php endif; ?>


<?php
foreach($terms as $category):?>
	<?php include(locate_template('woocommerce/custom-templates/content-featured-product.php')); ?>
	<section class="view-more category-<?=$category->slug;?>">
  <?php $button = array('title' => 'View More', 'target' => '', 'url' => get_term_link($category));
        include(locate_template('partials/button.php'));?>
   </section>	
<?php endforeach;?>