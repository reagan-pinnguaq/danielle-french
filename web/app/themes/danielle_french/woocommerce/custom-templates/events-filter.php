<?php 
/**
 * Events filter template. 
 * creates a set of inputs that can used to 
 * filter products that are also generated based on cateory type
 */
 global $post;
 $seasons = array('winter', 'spring', 'summer', 'fall'); //assumed default seasons
 $types = array('events', 'workshops');

 $filters = array_merge($types, $seasons);
 ?>
<section class="filter-products" id="#filter">
	<!-- buttons/labels -->
	<div class="filter-wrapper">
		<?php foreach($filters as $filter):?><label class="btn btn-featured" for="<?=$filter;?>"><?=ucwords($filter);?></label><?php endforeach;?>
	</div>
	<!-- inputs -->
	<?php $k=0; foreach($filters as $filter):?><input type="radio" id="<?=$filter;?>" name="filter" value="<?=$filter;?>" <?=($k==0)? 'checked': '';?>><?php $k++; endforeach;?>
	
	<?php // do all
	foreach($filters as $filter):
	$args = ($filter == 'events' || $filter == 'workshops') ?  get_product_args_array(false, $filter, false, true)	: get_product_args_array(false, false, $filter, false);
	$products = new WP_Query($args);
	
	if ( $products->have_posts() ) : ?>
		<ul class="product-collection card <?=$filter;?>">
			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
			<?php wc_get_template_part( 'content', 'product' ); ?>
			<?php endwhile;?> 
		</ul>
	<?php endif; wp_reset_postdata(); ?>
	<?php endforeach;?>

	<a class="mobile-only btn" href="#filter">back to filters</a>
</section>

