
<section class="featured-products">
	<h3>Featured <?=str_replace('ss','s', $category->name . 's');?></h3>
	<?php //fetch and display content.
	$args = get_featured_product_args($category); 

	$products = new WP_Query($args);
	
	if ( $products->have_posts() ) : ?>
		<ul class="product-archive">
		<?php while ( $products->have_posts() ) : $products->the_post(); ?>
			<?php wc_get_template_part( 'content', 'product' ); ?>
		<?php endwhile; // end of the loop. ?> 
		</ul>
	<?php endif;?>
	<?php wp_reset_postdata(); ?>
</section>