	<?php foreach($terms as $term):?>
	
	<div class="card">
		<a href="	<?=get_term_link($term);?>" alt="">
		<?php $image = get_field( sanitize_title($term->name) .'_image',get_page_by_title('shop')); $image['limit'] = true;?>
		<?php include(locate_template('partials/picture.php'));?>
		</a>
		<h3><a href="<?=get_term_link($term);?>" alt=""><?=str_replace('ss', 's', $term->name .'s')?></a></h3>
	</div>
<?php endforeach;?>