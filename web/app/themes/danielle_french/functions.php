<?php


// var_dump(get_plugins());
// die();
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'vendor/autoload.php',                    //AutoLoads anything brought in by composer.

  'lib/config/assets.php',                 // Scripts and stylesheets
  'lib/config/setup.php',                  // Theme setup
  'lib/config/extras.php',                 // Theme funciton extenstion

  'lib/header-content.php',                // Page titles + header images
  'lib/wrapper.php',                       // Theme wrapper class
  'lib/customizer/index.php',              // Theme customizer
  'lib/config/wooCommerce.php',            // WooCommerce Support
  'lib/wooCalendar.php',
  'lib/custom.php',                        // Custom functions

  'lib/walker/custom_navwalker.php',       
  'lib/walker/nav_footer_walker.php',
  'lib/walker/simplified_navwalker.php',

  'lib/config/acf_export.php',              //Hard coded acf inputs with restricted access
  'lib/ajax.php',                           //Ajax handler for dynamic paginate delievery
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  } 
  require_once $filepath;
} 
unset($file, $filepath);
