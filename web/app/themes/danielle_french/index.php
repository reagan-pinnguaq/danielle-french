<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<section class="card-block grid three posts-recent" <?php if(is_category()):?>data-category="<?=get_the_category()[0]->slug;?>"<?php endif;?> >
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
  <?php endwhile; ?>
  
  <?php //the_posts_navigation(); ?>
</section>
<section class="load-more">
  <a href="#" id="post" class="load-more-posts load-more-btn btn">Load More</a>
</section>
<?php if (get_option('page_for_posts', true) && is_home()):?>
  <!-- Default blog archive only -->
  <?php get_template_part('templates/content-category');?>
<?php endif;?>