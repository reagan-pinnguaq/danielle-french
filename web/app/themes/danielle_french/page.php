

<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}?>

<?php get_template_part('partials/building');?>
<?php if(is_page('events')):?>
	<?php get_template_part('woocommerce/custom-templates/calendar'); ?>
<?php endif;?>

<?php if(is_page('media')):
	$fields = array(
		'show' 			=> 'before_show_content', 
		'press' 		=> 'before_press_content', 
		'gallery' 	=> 'before_gallery_content'
	);
	foreach($fields as $type => $field_id):?>
	<?php $content_id = $field_id;
		include(locate_template('templates/blocks/content_block.php'));?>
		<ul class="card-block grid three press-media-content <?=$type;?>">
		<?php include(locate_template('templates/content-press.php'));?>
		</ul>
		<section class="load-more">
			<a href="#" id="<?=$field_id;?>" class="load-more-press load-more-btn btn">Load More</a>
		</section>
<?php endforeach;
endif;?>

<?php while (have_posts()) : the_post(); //content loop?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<?php if(is_page('contact')):?>
	<?php get_template_part('partials/contact-form');?>
<?php endif;?>

<?php
/**
 * If you are looking for woocommerce specific page template code check the wooCommerce folder for overides.
 * 
 * In a wooCommerce context do the appropriate action.
 * This is done because the content editor for the page
 * is ignored by default in favour of content blocks. 
 * This re adds the short codes to get the functionality back.
 * 
 */
if(is_cart()):?>
<section class="cart">
	<?=do_shortcode('[woocommerce_cart]');?>
</section>
<?php elseif(is_checkout()):?>
<section class="checkout">
	<?=do_shortcode('[woocommerce_checkout]');?>
</section>
<?php elseif(is_account_page()):?>
<section class="account">
	<?=do_shortcode('[woocommerce_my_account]');?>
</section>
<?php endif;?>





