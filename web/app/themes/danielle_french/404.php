<?php get_template_part('templates/page', 'header'); ?>
<section class="no-result">
  <div class="alert alert-warning">
    <h2>Whoops! Something isn't right.</h2>
    <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
</section>


