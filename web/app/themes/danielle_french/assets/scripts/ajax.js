(function($) {
    //offsets
    var showOffset = 1;
    var pressOffset = 1;
    var galleryOffset = 1;
    var postOffset = 1;

    function increment(offset_id) {
        if (offset_id === 'show') {
            showOffset++;
        } else if (offset_id === 'press') {
            pressOffset++;
        } else if (offset_id === 'gallery') {
            galleryOffset++;
        } else if (offset_id === 'post') {
            return postOffset++;
        }

    }

    function get_offset(offset_id) {
        if (offset_id === 'show') {
            return showOffset;
        } else if (offset_id === 'press') {
            return pressOffset;
        } else if (offset_id === 'gallery') {
            return galleryOffset;
        } else if (offset_id === 'post') {
            return postOffset;
        }
    }

    $('.load-more-press').unbind('click').bind('click', function(e) {
        e.preventDefault();

        id = $(this).attr('id');
        offset_id = id.replace('_content', '').replace('before_', '');
        offset = get_offset(offset_id);
        container = $(this).parent().prev();
        $.ajax({
            url: enqueData.ajax_url,
            type: 'POST',
            data: {
                action: 'pressPostFetch',
                offset: offset,
                item_type: offset_id,
                id: id,
            },
            success: function(response) {
                if (response) {
                    $(container).append(response);
                    increment(offset_id);
                    console.log('here');
                } else {
                    $(this).parent().remove();
                    $(this).parent().append('<p>Sorry, we ran out of content</p>');

                }
            }
        });
    });

    $('.load-more-posts').unbind('click').bind('click', function(e) {
        e.preventDefault();

        offset_id = $(this).attr('id');
        offset = get_offset(offset_id);
        container = $('.posts-recent');
        category = $(container).attr('data-category');
        $.ajax({
            url: enqueData.ajax_url,
            type: 'POST',
            data: {
                action: 'blogPostFetch',
                offset: offset,
                category: category,
            },
            success: function(response) {
                if (response && response !== 'false') {
                    $(container).append(response);
                    increment(offset_id);
                } else {
                    $(this).parent().remove();
                    $(container).append('<p class="notice">Sorry, we ran out of content</p>');
                }
            },
        });
    });
})(jQuery);