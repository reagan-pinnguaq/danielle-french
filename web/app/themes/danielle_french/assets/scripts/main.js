/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {
  //mail registration.
  function sendPackage(list) {
    list = list;
    $(document).ready(function() {
      $("a.request-information-package").click(function(event) {
        //Create elements for handling.
        event.preventDefault();
        var form = document.createElement("form");
        form.setAttribute("id", "request-package");
        form = this.parentElement.appendChild(form);
        var inputEmail = document.createElement("INPUT");
        inputEmail.setAttribute("type", "email");
        inputEmail.setAttribute("required", "required");
        inputEmail.setAttribute("placeholder", "Your Email");
        var inputSubmit = document.createElement("INPUT");
        inputSubmit.setAttribute("type", "submit");
        inputSubmit.setAttribute("value", "Send Info Package");
        messagebox = document.createElement("div");
        messagebox.setAttribute("id", "response-box");
        form.appendChild(inputEmail);
        form.appendChild(inputSubmit);
        form.appendChild(messagebox);
        $(this).addClass("sr-only");
      });
      //listen for form creation then listen for the submission.
      $("section").on("DOMNodeInserted", "form", function() {
        //unbind and rebind to prevent multi firing.
        $("#request-package")
          .unbind("submit")
          .bind("submit", function(event) {
            event.preventDefault();
            var email = $('input[type = "email"]').val();
            $("");
            $("input").addClass("sr-only");
            $.ajax({
              url: enqueData.ajax_url,
              type: "POST",
              data: {
                action: "mailingSignUp",
                list: list,
                email: email
              },
              success: function(response) {
                if (response) {
                  // console.log('true');
                  $("#response-box").append(
                    "<p>A pdf download has been sent to the email provided.</p>"
                  );
                  $("#response-box p").addClass("success");
                } else {
                  // console.log('false');
                  $("#response-box").append(
                    '<p class="error">Were sorry we could not send a package out at this time. Please try again later</p>'
                  );
                  $("#response-box p").addClass("error");
                }
              },
              error: function(response) {
                $("#response-box").append(
                  '<p class="error">Were sorry we could not send a package out at this time. Please try again later</p>'
                );
                $("#response-box p").addClass("error");
                console.log();
              }
            });
          });
      });
    });
  }

  var Sage = {
    common: {
      // All pages
      finalize: function() {
        $(".carousel-block").slick({
          dots: false,
          arrows: true,
          autoplay: true,
          speed: 800,
          adaptiveHeight: true
        });
        $(".slide").removeAttr("style");
        $('a[href="#"]').click(function(e) {
          e.preventDefault();
        });

        $(".slick-prev").html("&#10094;");
        $(".slick-next").html("&#10095;");
      }
    },
    contact: {
      finalize: function() {
        $("#contact-form")
          .unbind("submit")
          .bind("submit", function(event) {
            event.preventDefault();
            $(this).addClass("collapsed");
            $.ajax({
              url: enqueData.ajax_url,
              type: "POST",
              data: {
                action: "contactPostAction",
                fname: $('input[name="first"]').val(),
                lname: $('input[name="last"]').val(),
                email: $('input[name="email"]').val(),
                subject: $("select").val(),
                body: $('textarea[name="message"]').val(),
                feedback: $('input[name="hear"]').val(),
                subscribe: $('input[name="newsletter"]:checked').val()
              },
              success: function(response) {
                if (response) {
                  $(this)
                    .parent()
                    .append(
                      '<div class="success"><p>Your Message was sent. Somone will reply to you shortly.</p></div>'
                    );
                } else {
                  $(this)
                    .parent()
                    .append(
                      '<div class="error"><p>We are sorry, we could not process your message at this time. <a href="mailto:connect@southpondfarms.ca"> send us an email</a> </p></div>'
                    );
                }
              },
              error: function(response) {
                $(this)
                  .parent()
                  .append(
                    '<div class="error"><p>We are sorry, we could not process your message at this time. <a href="mailto:connect@southpondfarms.ca"> send us an email</a> </p></div>'
                  );
              }
            });
          });
      }
    },
    weddings: {
      finalize: function() {
        list = "weddings";
        sendPackage(list);
      }
    },

    corporate: {
      finalize: function() {
        list = "corporate";
        sendPackage(list);
      }
    },
    events: {
      finalize: function() {
        $(".calendar-grid>li>p>a")
          .unbind("click")
          .bind("click", function() {
            parent = $(this)
              .parent()
              .parent();
            //reveal the child and child content
            $(parent)
              .toggleClass("collapsed")
              .toggleClass("full-item");
            $(parent)
              .find("ul")
              .toggleClass("collapsed");
            $(parent)
              .find("p")
              .toggleClass("collapsed");
            $(".calendar-grid>li").toggleClass("collapsed");
            window.location.hash = "#calendar";
          });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = funcname === undefined ? "init" : funcname;
      fire = func !== "";
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === "function";

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire("common");

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, "_").split(/\s+/), function(
        i,
        classnm
      ) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, "finalize");
      });

      // Fire common finalize JS
      UTIL.fire("common", "finalize");
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

  $(document).ready(function() {
    // mobile nav
    $("#mobile-nav").click(function() {
      $(this).toggleClass("is-active");
      $("#nav-primary").toggleClass("mobile-collapsed");
      $("body").toggleClass("frozen");
    });

    // blog filter
    $(".post-filter label").click(function() {
      $(".post-filter label").removeClass("active-btn");
      $(this).addClass("active-btn");
    });
  });
})(jQuery);
